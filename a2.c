#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include "a2_helper.h"
#include <stdlib.h>
#include <pthread.h>
#include <fcntl.h>  
#include <semaphore.h>
#include <stdbool.h>

sem_t can_start_t3;
sem_t can_end_t2;
sem_t run_at_the_same_time;
sem_t can_end;
sem_t *can_start_p7t4;
sem_t *can_start_p4t1;

pthread_barrier_t barrier;

int n = 0;

bool th11_running = false;

#define NR_TH_P5 48
#define NR_TH_P7 4
#define NR_TH_P4 6

void P(sem_t *sem)
{
    sem_wait(sem);
}

void V(sem_t *sem)
{
    sem_post(sem);
}

void P2(sem_t ** sem)
{
    sem_wait(*sem);
}

void V2(sem_t **sem)
{
    sem_post(*sem);
}

void* p7_function(void *args)
{
    int threadNo = *(int *)args;
    
    if(threadNo == 4){
        P2(&can_start_p7t4);
    }

    if(threadNo == 3)
       P(&can_start_t3);

    info(BEGIN, 7, threadNo);

    if(threadNo == 2){
        V(&can_start_t3);
        P(&can_end_t2);
    }
    
    info(END, 7, threadNo);

    if(threadNo == 3)
        V(&can_end_t2);

    if(threadNo == 4){
        V2(&can_start_p4t1);
    }
    
    return NULL; 

}

void* p5_function(void *args)
{
    int threadNo = *(int *) args;

    P(&run_at_the_same_time);

    info(BEGIN, 5, threadNo);
    /*
    if(threadNo == 11){
        th11_running = true;
        sem_getvalue(&run_at_the_same_time, &n);
        if(n != 0)
            pthread_barrier_init(&barrier, NULL, n);
    }
    else{
        if(th11_running){
            pthread_barrier_wait (&barrier);
            P(&can_end);
        }
    }
    */
    info(END, 5, threadNo);
    /*
    if(threadNo == 11){
        V(&can_end);
        th11_running = false;
        pthread_barrier_destroy(&barrier);
       
    }
    */
    V(&run_at_the_same_time); 

    return NULL;

}

void* p4_function(void *args){

    int threadNo = *(int *) args;

    if(threadNo == 1){
        P2(&can_start_p4t1);
    }

    info(BEGIN, 4, threadNo);

    info(END, 4, threadNo);

    if(threadNo == 4){
        V2(&can_start_p7t4);
    }

    return NULL;

}

int main(int argc, char **argv)
{

    init();
   
    info(BEGIN, 1, 0);

    int p2Pid = fork();

    switch(p2Pid){

        case -1: 
            perror("Not created");
            break;

        case 0:  //P2
            info(BEGIN, 2, 0);

            info(END, 2, 0);
            break;

        default : ;// P1

            int p3Pid = fork();

            switch(p3Pid){
                case -1:
                    perror("Not created");
                    break;

                case 0: ; //P3
                    info(BEGIN, 3, 0);

                    int p5Pid = fork();

                    switch (p5Pid)
                    {
                        case -1:
                            perror("Can t create p5");
                            break;
                        case 0: //P5
                            info(BEGIN, 5, 0);

                            pthread_t p5_threads[NR_TH_P5];
                            int args_p5[NR_TH_P5];

                            if(sem_init(&run_at_the_same_time, 1, 5) < 0 || sem_init(&can_end, 1, 0) < 0 ){
                                 perror("Semaphores p5 not created!");
                                    exit(3);
                            }

                            for(int i = 0 ; i < NR_TH_P5 ; i++){
                                args_p5[i] = i+1;
                                if(pthread_create(&p5_threads[i], NULL, p5_function, &args_p5[i]) != 0){
                                    perror("Can t create process 5 threads");
                                    exit(4);
                                }
                            }
                         
                            for(int i  = 0 ; i < NR_TH_P5 ; i++)
                                pthread_join(p5_threads[i], NULL);

                            //sem_destroy(&run_at_the_same_time);

                            info(END, 5, 0);

                            break;

                        default: //
                        
                            waitpid(p5Pid, NULL, 0);

                            int p6Pid = fork();

                            switch (p6Pid)
                            {
                                case -1:
                                    perror("Cannot create");
                                    break;
                                
                                case 0: ; // P6

                                    info(BEGIN, 6, 0);

                                    int p7Pid = fork();

                                    switch (p7Pid)
                                    {
                                        case -1:
                                            perror("Can t create");
                                            break;
                                        
                                        case 0:  //P7
                                            info(BEGIN, 7, 0);

                                            pthread_t p7_threads[NR_TH_P7];
                                            int args_p7[NR_TH_P7];

                                            can_start_p7t4 = sem_open("/T7.4_start", 0);
                                            can_start_p4t1 = sem_open("/T4.1_start", O_CREAT, 0644, 0);

                                            if(sem_init(&can_end_t2, 1, 0) < 0  || sem_init(&can_start_t3, 1, 0) < 0){
                                                perror("Semaphores p7 not created!");
                                                exit(1);
                                            }

                                            for(int i = 0; i < NR_TH_P7 ; i++){
                                                args_p7[i] = i+1;
                                                if(pthread_create(&p7_threads[i], NULL, p7_function, &args_p7[i]) != 0){
                                                    perror("Can t create process 7 threads");
                                                    exit(2);
                                                }
                                            }

                                            for(int i = 0 ; i < NR_TH_P7 ; i++){
                                                 pthread_join(p7_threads[i], NULL);
                                            }

                                            sem_destroy(&can_end_t2);
                                            sem_destroy(&can_start_t3);

                                            info(END, 7, 0);
                                            break;

                                        default:  //P6

                                            waitpid(p7Pid, NULL, 0);

                                            info(END, 6, 0);
                                            break;
                                    }

                                    break;

                                default: //P3

                                    waitpid(p6Pid, NULL, 0);
                                    info(END, 3, 0);
                                    break;
                            }
                            break;
                    } 
                    break;

                default: ;//P1

                    int p4Pid = fork();

                    switch (p4Pid)
                    {
                        case -1:
                            perror("Cant create p4");
                            break;
                        case 0: //P4

                            info(BEGIN, 4, 0);

                            can_start_p7t4 = sem_open("/T7.4_start", O_CREAT, 0644 , 0);
                            can_start_p4t1 = sem_open("/T4.1_start", 0);

                            pthread_t p4_threads[NR_TH_P4];
                            int args_p4[NR_TH_P4];

                            for(int i = 0; i < NR_TH_P4 ; i++){
                                args_p4[i] = i+1;
                                if(pthread_create(&p4_threads[i], NULL, p4_function, &args_p4[i]) != 0){
                                    perror("Can t create process 4 threads");
                                    exit(2);
                                }
                            }

                            for(int i = 0 ; i < NR_TH_P4 ; i++){
                                    pthread_join(p4_threads[i], NULL);
                            }

                            info(END, 4, 0);
                            break;
                        
                        default: //P1

                            for (int child = 0; child < 3; child++) {
                                int status;
                                wait(&status);
                            }

                            info(END, 1, 0);
                            break;
                    }

                    break;
            }
            
            break;
    }  
    //sem_close(can_start_p4t1);
    //sem_close(can_start_p7t4);  
    return 0;
}
